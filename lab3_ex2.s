	.file	1 "lab3_ex2.c"
	.section .mdebug.eabi32
	.previous
	.section .gcc_compiled_long32
	.previous
	.gnu_attribute 4, 1
	.text
	.align	2
	.globl	main
	.set	nomips16
	.ent	main
	.type	main, @function
main:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lui	$6,%hi(source) # higher half of the source is put to a2
	lw	$3,%lo(source)($6) # v1 = lower half of the source
	beq	$3,$0,$L2 # if v1 == zero then you go to L2
	lui	$7,%hi(dest) # higher half of destination is at a3 
	addiu	$7,$7,%lo(dest) # lower half of desitnation is added to a3 
	addiu	$6,$6,%lo(source) # lower half of the source is added to a2
	move	$2,$0 #v0 = zero
$L3:
	addu	$5,$7,$2 # a1 = a3 + v0
	addu	$4,$6,$2 # a0 = a2 + v0
	sw	$3,0($5) # store A[0](v1) = a1 
	lw	$3,4($4) # load A[0](v1) = a0
	addiu	$2,$2,4 # v0 = v0 + 4
	bne	$3,$0,$L3 # if v1 != zero then go to L3
$L2:
	move	$2,$0 # if v1 == zero then you move v0 = zero
	j	$31 #jump to ra
	.end	main
	.size	main, .-main
	.globl	source
	.data
	.align	2
	.type	source, @object
	.size	source, 28
source:
	.word	3
	.word	1
	.word	4
	.word	1
	.word	5
	.word	9
	.word	0

	.comm	dest,40,4
	.ident	"GCC: (GNU) 4.4.1"
